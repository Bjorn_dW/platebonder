#include "Mimetas.h"
#include "Arduino.h"
//#include "Adafruit_MCP23017.h"
//#include "Adafruit_RGBLCDShield.h"

Mimetas::Mimetas()
{
	//_time = time;
}

void Mimetas::build(){

  Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();
  lcd.clear();
  byte car1 [8] = {0b01111,0b11111,0b11111,0b01111,0b00000,0b00000,0b01111,0b11111};
  byte car2 [8] = {0b11111,0b11111,0b11111,0b11111,0b00000,0b00000,0b11110,0b11111};
  byte car3 [8] = {0b11110,0b11111,0b11111,0b11110,0b00000,0b00000,0b00000,0b00000};
  byte car4 [8] = {0b11111,0b01111,0b00000,0b00000,0b01111,0b11111,0b11111,0b01111};
  byte car5 [8] = {0b11111,0b11110,0b00000,0b00000,0b11111,0b11111,0b11111,0b11111};
  byte car6 [8] = {0b00000,0b00000,0b00000,0b00000,0b11110,0b11111,0b11111,0b11110};

  lcd.createChar(1,car1); delay(20); lcd.createChar(2,car2); delay(20); lcd.createChar(3,car3); delay(20);
  lcd.createChar(4,car4); delay(20); lcd.createChar(5,car5); delay(20); lcd.createChar(6,car6); delay(20);
}

void Mimetas::logo(){
  Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();
  lcd.begin(16,2);
  lcd.setCursor(16,0); lcd.write(1); delay(10); lcd.write(2); delay(10); lcd.write(3); delay(10);
  lcd.setCursor(16,1); lcd.write(4); delay(10); lcd.write(5); delay(10);  lcd.write(6); delay(10);
  for (int positionCounter = 0; positionCounter < 16; positionCounter++) {
    lcd.scrollDisplayLeft(); delay(150);
  }
  lcd.setCursor(25,0); lcd.print("Mimetas"); lcd.setCursor(20,1); lcd.print("pRocker v1.0");
  delay(2000);
}
