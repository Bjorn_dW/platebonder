/*
 * Project plateBonder
 * Description: Operating the plateBonder, PID controlled temperature control
 * Author: Bjorn de Wagenaar
 * Date: 191017
 */
 SYSTEM_MODE(AUTOMATIC);  // Photon particle automaticcally connects to the cloud
 //SYSTEM_MODE(SEMI_AUTOMATIC); // Offline mode

 /*****************************************************************
                         Include libraries
 *****************************************************************/
 #include "math.h"
 #include "Adafruit_MCP23017.h"
 #include "Adafruit_RGBLCDShield.h"

 /*****************************************************************
                   A/D pin & adress definitions
 *****************************************************************/
 #define PIN_SDA         0       // SDA pin I2C
 #define PIN_SCL         1       // SCL pin I2C
 #define PIN_ENB         2       // End switch 1
 #define PIN_IN3         3       // End switch 2
 #define PIN_IN4         4       // Motor pin direction
 #define PIN_LED         5       // Motor pin direction
 #define PIN_VALVE       7       // Motor pin step
 #define PIN_BUTTON1     A0      // Encoder 1 pin 1
 #define PIN_BUTTON2     A1      // Encoder 1 pin 2
 #define PIN_THERM       A2      // Encoder 1 pin switch

double temp;
double Vtherm;
double Rref = 9960;

 Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();


void setup() {

  Serial.begin(9600);
  lcd.begin(16, 2);

  // Defining pinMode motor pins, well volume sensors
  pinMode(PIN_ENB, OUTPUT);     //
  pinMode(PIN_IN3, OUTPUT);     //
  pinMode(PIN_IN4, OUTPUT);     //
  pinMode(PIN_LED, OUTPUT);     //
  pinMode(PIN_VALVE, OUTPUT);   //

  pinMode(PIN_BUTTON1, INPUT);
  pinMode(PIN_BUTTON2, INPUT);
  pinMode(PIN_THERM, INPUT);
}


void loop() {

  uint8_t buttons = lcd.readButtons();

  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("plateBonder");
  lcd.setCursor(0,1);
  lcd.print("Temp = ");
  Vtherm = analogRead(PIN_THERM);
  temp = -25*log(Vtherm*9960/(4095-Vtherm)/26828);
  //temp = -25*log(1/26828*(Vtherm*Rref/(4095-Vtherm)));

  lcd.print(temp);

  if (analogRead(PIN_BUTTON1) < 100 && analogRead(PIN_BUTTON2) < 100){
    digitalWrite(PIN_VALVE, HIGH);
    digitalWrite(PIN_LED, HIGH);
    delay(250);
  }
  if (analogRead(PIN_BUTTON1) > 100 || analogRead(PIN_BUTTON2) > 100){
    digitalWrite(PIN_VALVE, LOW);
    digitalWrite(PIN_LED, LOW);
    delay(250);
  }

}
