
/*
 * Project plateBonder
 * Description: Operating the plateBonder, PID controlled temperature control
 * Author: Bjorn de Wagenaar
 * Date: 191017
 */

  /*****************************************************************
                         Include libraries
 *****************************************************************/

 #include "math.h"                           // Math calculation library
 #include "Adafruit_MCP23017.h"              // LCD library
 #include "Adafruit_RGBLCDShield.h"          // LCD library
 #include "pid.h"                            // PID library
 #include "MenuBackend.h"                    // Menu library

 /*****************************************************************
                   A/D pin & adress definitions
 *****************************************************************/

 #define PIN_SDA               0             // SDA pin I2C display & buttons
 #define PIN_SCL               1             // SCL pin I2C display and buttons
 #define PIN_ENB               2             // ENB PWM pin to motor driver
 #define PIN_IN3               3             // IN3 pin to motor driver
 #define PIN_IN4               4             // IN4 pin to motor driver
 #define PIN_LED               5             // LED pin for LEDS button 1 & 2
 #define PIN_VALVE             7             // Valve pin
 #define PIN_BUTTON1           A0            // Button 1
 #define PIN_BUTTON2           A1            // Button 2
 #define PIN_THERM             A2            // Thermistor pin

 /*****************************************************************
                       Variable declaration
 *****************************************************************/

  double temp, Vtherm, Setpoint, Output, Rref = 9960;             // PID variables
  int pTime, lastButtonPushed, varselect, buttonPush = 200, refreshInterval = 500, tempTolerance = 2;
  unsigned long displayMillis, currentMillis, pumpMillis;
  bool initiate = 0, menuSelect = 0, boolPump = 0;
  String Str, Str2;

  // Custom characters for Mimetas intro
  byte car1 [8] = {0b01111,0b11111,0b11111,0b01111,0b00000,0b00000,0b01111,0b11111};
  byte car2 [8] = {0b11111,0b11111,0b11111,0b11111,0b00000,0b00000,0b11110,0b11111};
  byte car3 [8] = {0b11110,0b11111,0b11111,0b11110,0b00000,0b00000,0b00000,0b00000};
  byte car4 [8] = {0b11111,0b01111,0b00000,0b00000,0b01111,0b11111,0b11111,0b01111};
  byte car5 [8] = {0b11111,0b11110,0b00000,0b00000,0b11111,0b11111,0b11111,0b11111};
  byte car6 [8] = {0b00000,0b00000,0b00000,0b00000,0b11110,0b11111,0b11111,0b11110};

 Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();      //Initiate diplay lib
 PID myPID(&temp, &Output, &Setpoint,2,5,1, PID::DIRECT);  //Initiate PID lib

 SYSTEM_MODE(AUTOMATIC);  // Photon particle automaticcally connects to the cloud
 //SYSTEM_MODE(SEMI_AUTOMATIC); // Offline mode

 /*****************************************************************
                  MenuBackend (menustructure)
 *****************************************************************/

MenuBackend menu = MenuBackend(menuUseEvent,menuChangeEvent);

MenuItem Item1= MenuItem("Temperature");
MenuItem Item1Sub1 = MenuItem("Temp =");

MenuItem Item2 = MenuItem("PumpTime");
MenuItem Item2Sub1 = MenuItem("Time =");

MenuItem Item3= MenuItem("Firmware update");
MenuItem Item3Sub1 = MenuItem("Update mode");

MenuItem Item4= MenuItem("Info");
MenuItem Item4Sub1 = MenuItem("Version 1.0");

MenuItem Item5= MenuItem("Credits");
MenuItem Item5Sub1 = MenuItem("Mimetas");

 //this function builds the menu and connects the correct items together
void menuSetup(){
     Serial.println("Setting up menu...");   //add the file menu to the menu root
     menu.getRoot().add(Item1); //setup the settings menu item   //Item 1...
     Item1.addRight(Item2).addRight(Item3).addRight(Item4).addRight(Item5);
     //Item 1...
     Item1.add(Item1Sub1);
     //Item 2...
     Item2.add(Item2Sub1);
     //Item 3...
     Item3.add(Item3Sub1);
     //Item 4...
     Item4.add(Item4Sub1);
     //Item 5...
     Item5.add(Item5Sub1);
 }

 /*****************************************************************
                   Use item menu (MenuBackend)
 *****************************************************************/

void menuUseEvent(MenuUseEvent used){

     bool test = true;
     lcd.setCursor(0,1);


     if (used.item == Item1Sub1){ //comparison agains a known item
         lcd.print("> Set = "); lcd.print(Setpoint); lcd.print((char)223);
         delay(1250);
         menu.toRoot();
     }

     if (used.item == Item2Sub1){ //comparison agains a known item
         lcd.print("> Time = "); lcd.print(pTime); lcd.print(" "); lcd.print("s");
         delay(1250);
         menu.toRoot();
     }

     if (used.item == Item5Sub1){ //comparison agains a known item
         Particle.connect();
         lcd.print("-> Update mode");
         delay(1250);
         menu.toRoot();
     }

 }

 /*****************************************************************
                  Menu navigation (MenuBackend)
 *****************************************************************/

void menuChangeEvent(MenuChangeEvent changed){
     MenuItem newMenuItem=changed.to; //get the destination menu
       // (changed.from.getName());
       // (changed.to.getName())

     lcd.clear();
     lcd.setCursor(0,1);
     lcd.print(changed.to.getName());
     lcd.setCursor(0,0);

     if(newMenuItem.getName()==menu.getRoot()){
         lcd.setCursor(0,0);
         lcd.print("plateBonder");
         menuSelect = 0;
     }

     else if(newMenuItem.getName()=="Temperature" || newMenuItem.getName()=="PumpTime" || newMenuItem.getName()=="Firmware update" || newMenuItem.getName()=="Info" || newMenuItem.getName()=="Credits"){
         lcd.print("    plateBonder");
         menuSelect = 1;
     }

     else if(newMenuItem.getName()=="Temp ="){
         lcd.print("     Temperature");
         varselect = 1;
         Setpoint = change (Setpoint);
         menuSelect = 1;
     }

     else if(newMenuItem.getName()=="Time ="){
         lcd.print("        PumpTime");
         varselect = 2;
         pTime = change (pTime);
         menuSelect = 1;
     }

     else if(newMenuItem.getName()=="Update mode"){
         lcd.print(" Firmware update");
         menuSelect = 1;
     }

     else if(newMenuItem.getName()=="Version 1.0"){
         lcd.print("V1.0 20190723 BW");
         menuSelect = 1;
     }
     else if(newMenuItem.getName()=="Mimetas"){
         lcd.print("         Credits");
         menuSelect = 1;
     }
}

 /*****************************************************************
                         Read LCD buttons
 *****************************************************************/

void ReadButtons(){  //read buttons status

    lastButtonPushed = 0;
    uint8_t buttons = lcd.readButtons();

    if (buttons) {
        if (buttons & BUTTON_UP){
            lastButtonPushed = 2;
            delay(buttonPush);
        }
        if (buttons & BUTTON_DOWN){
            lastButtonPushed = 8;
            delay(buttonPush);
        }
        if (buttons & BUTTON_LEFT){
            lastButtonPushed = 4;
            delay(buttonPush);
        }
        if (buttons & BUTTON_RIGHT){
            lastButtonPushed = 6;
            delay(buttonPush);
        }
        if (buttons & BUTTON_SELECT){
            lastButtonPushed = 9;
            delay(buttonPush);
        }
    }
}

 /*****************************************************************
              Navigate through menu (MenuBackend)
 *****************************************************************/

void NavigateMenu(){
    //Serial.println("VOID NavigateMenu");
    //Serial.println(lastButtonPushed);
    MenuItem currentMenu=menu.getCurrent();

    if (lastButtonPushed != 0){// so if nothing happend then don't waste it's time
        switch (lastButtonPushed) {
            case 8: menu.moveRight(); Serial.println("Case DOWN"); break;
            case 2: menu.moveLeft(); Serial.println("Case UP"); break;
            case 9: menu.use(); Serial.println("Case SELECT"); break;

            case 4: //menu.moveBack(); break;
                if ((currentMenu.moveUp()))
                    {menu.moveUp(); Serial.println("Case LEFT"); break;}
                else
                    {menu.moveBack(); Serial.println("Case LEFT"); break;}

            case 6:
                if ((currentMenu.moveDown()))
                menu.moveDown(); Serial.println("Case RIGHT"); break;
        }
    }
}

 /*****************************************************************
               Change parameter values (MenuBackend)
 *****************************************************************/

 double change (double variable){
     Serial.println("VOID Change");

     if (varselect == 1){
         variable = Setpoint;
         Str = "Set = " ;
         Str2 = (char)223;
     }
     else if (varselect == 2){
         variable = pTime;
         Str = "Time = ";
         Str2 = " s";
     }

     while (true){
         lcd.setCursor(0,1);
         lcd.print(Str); lcd.print(variable,1); lcd.print(Str2);

         uint8_t buttons = lcd.readButtons();

         if (buttons) {
             if (buttons & BUTTON_UP){
                 if (varselect == 1){
                     variable = constrain(variable,20,60);
                     variable = variable + 1;
                     initiate = 0;
                 }
                 else if (varselect == 2){
                     variable = constrain(variable,1,300);
                     variable = variable + 1;
                 }
                 delay(buttonPush);
             }
             if (buttons & BUTTON_DOWN){
                 if (varselect==1){
                     variable = constrain(variable,20,60);
                     variable = variable - 1;
                     Serial.println("Variable 1 DOWN");
                 }
                 else if (varselect==2){
                     variable = constrain(variable,1,300);
                     variable = variable - 1;
                     Serial.println("Variable 2 DOWN");
                 }
                 delay(buttonPush);
             }

             if (buttons & BUTTON_SELECT){
                 return variable;
             }
         }
     }
 }


 /*****************************************************************
                              Void Setup
 *****************************************************************/

void setup(){

  Serial.begin(9600);                                   // Setup serial com
  lcd.begin(16, 2);                                     // Setup LCD
  myPID.SetMode(PID::AUTOMATIC);                        // Setup PID

  pinMode(PIN_ENB, OUTPUT);                             //
  pinMode(PIN_IN3, OUTPUT);                             //
  pinMode(PIN_IN4, OUTPUT);                             //
  pinMode(PIN_LED, OUTPUT);                             //
  pinMode(PIN_VALVE, OUTPUT);                           //

  pinMode(PIN_BUTTON1, INPUT);                          //
  pinMode(PIN_BUTTON2, INPUT);                          //
  pinMode(PIN_THERM, INPUT);                            //

  digitalWrite(PIN_IN3, HIGH);                          //
  digitalWrite(PIN_IN4, LOW);                           //
  digitalWrite(PIN_LED, LOW);                           //
  digitalWrite(PIN_VALVE, HIGH);                        //

  Setpoint = 25;
  pTime = 30;

  mimetasIntro();
  tempRead();
  menuSetup();            // menuSetup (backendMenu)
  menu.toRoot();          //
  displayMillis = millis();
}

/*****************************************************************
                             Void Loop
*****************************************************************/

void loop() {

  currentMillis = millis();
  ReadButtons();
  NavigateMenu();

  tempRead();
  myPID.Compute();
  analogWrite(PIN_ENB, Output);

  mainmenuDisplay();
  pumpProcess();
}

/*****************************************************************
                          Void tempRead
*****************************************************************/

void tempRead(){

  // Read-out of analog pin 0 - 3.3V with 12 bit ADC --> values between 0 - 4095. A voltage divider with a Rref of 9960 Ohms is used
  // Vtherm = Vout * Rref / (Vin - Vout) in which the V values are the values for the voltage converted using the 12 bit ADC.
  Vtherm = analogRead(PIN_THERM);
  // Relation between Resistance and temperature is a logaritmic realation:
  // Resistance = 26828 e ^ (-0.04 * temp) (fit in Excel R2 = 0.9991)
  temp = -25*log(Vtherm*9960/(4095-Vtherm)/26828);

}

/*****************************************************************
                        Void mimetasIntro
*****************************************************************/

void mimetasIntro (){

    lcd.createChar(1,car1); lcd.createChar(2,car2); lcd.createChar(3,car3);
    lcd.createChar(4,car4); lcd.createChar(5,car5); lcd.createChar(6,car6);
    lcd.setCursor(16,0); lcd.write(1); lcd.write(2); lcd.write(3);
    lcd.setCursor(16,1);lcd.write(4);  lcd.write(5);  lcd.write(6);
    for (int positionCounter = 0; positionCounter < 16; positionCounter++) {
        lcd.scrollDisplayLeft(); delay(150);
    }
    lcd.setCursor(25,0); lcd.print("Mimetas"); lcd.setCursor(21,1); lcd.print("plateBonder");
    delay(2000);

}

/*****************************************************************
                        Void mimetasIntro
*****************************************************************/

void mainmenuDisplay(){

    if (currentMillis-displayMillis > refreshInterval){
      lcd.setCursor(0,1);

      if (temp > (Setpoint-tempTolerance) && temp < (Setpoint+2) && menuSelect == 0 && boolPump == 0) {
          lcd.print("Ready     "); lcd.print(temp); lcd.print((char)223);
          digitalWrite(PIN_LED, HIGH);
          initiate = 1;
      }

      if (temp > (Setpoint-tempTolerance) && temp < (Setpoint+2) && menuSelect == 0 && boolPump == 1) {
          lcd.print("Running   "); lcd.print(temp); lcd.print((char)223);
          digitalWrite(PIN_LED, HIGH);
          initiate = 1;
      }

      if (temp < (Setpoint-tempTolerance) && initiate == 0 && menuSelect == 0) {
          lcd.print("Heating   "); lcd.print(temp); lcd.print((char)223);
          digitalWrite(PIN_LED, LOW);
      }

      if (temp > (Setpoint-tempTolerance) && initiate == 0 && menuSelect == 0) {
          lcd.print("Cooling   "); lcd.print(temp); lcd.print((char)223);
          digitalWrite(PIN_LED, LOW);

      }

    displayMillis = millis();
    }

}
/*****************************************************************
                        Void pumpProcess
*****************************************************************/

void pumpProcess(){

    if (analogRead(PIN_BUTTON1) < 100 && analogRead(PIN_BUTTON2) < 100 && boolPump == 0){
      digitalWrite(PIN_VALVE, LOW);
      pumpMillis = millis();
      boolPump = 1;
    }
    if (boolPump == 1 && (millis()-pumpMillis) > pTime*1000){
      digitalWrite(PIN_VALVE, HIGH);
      boolPump = 0;
      lcd.setCursor(0,1);
      lcd.print("    Process done");
      delay(2000);
    }

}
