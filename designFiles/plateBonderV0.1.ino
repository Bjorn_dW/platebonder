/*
 * Project plateBonder
 * Description: Operating the plateBonder, PID controlled temperature control
 * Author: Bjorn de Wagenaar
 * Date: 191017
 */

 SYSTEM_MODE(AUTOMATIC);  // Photon particle automaticcally connects to the cloud
 //SYSTEM_MODE(SEMI_AUTOMATIC); // Offline mode

 /*****************************************************************
                         Include libraries
 *****************************************************************/
 #include "math.h"
 #include "Adafruit_MCP23017.h"
 #include "Adafruit_RGBLCDShield.h"
 #include "pid.h"

 /*****************************************************************
                   A/D pin & adress definitions
 *****************************************************************/
 #define PIN_SDA         0       // SDA pin I2C display & buttons
 #define PIN_SCL         1       // SCL pin I2C display and buttons
 #define PIN_ENB         2       // ENB PWM pin to motor driver
 #define PIN_IN3         3       // IN3 pin to motor driver
 #define PIN_IN4         4       // IN4 pin to motor driver
 #define PIN_LED         5       // LED pin for LEDS button 1 & 2
 #define PIN_VALVE       7       // Valve pin
 #define PIN_BUTTON1     A0      // Button 1
 #define PIN_BUTTON2     A1      // Button 2
 #define PIN_THERM       A2      // Thermistor pin

  double temp;
  double Vtherm;
  double Rref = 9960;

  double Setpoint, Output;                         // PID variables


 Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();      //Initiate diplay lib
 PID myPID(&temp, &Output, &Setpoint,2,5,1, PID::DIRECT); //Initiate PID lib

void setup() {

  Serial.begin(9600);                                   // Setup serial com
  lcd.begin(16, 2);                                     // Setup LCD
  myPID.SetMode(PID::AUTOMATIC);                        // Setup PID

  tempRead();                                           //
  Setpoint = 50;                                        //

  pinMode(PIN_ENB, OUTPUT);                             //
  pinMode(PIN_IN3, OUTPUT);                             //
  pinMode(PIN_IN4, OUTPUT);                             //
  pinMode(PIN_LED, OUTPUT);                             //
  pinMode(PIN_VALVE, OUTPUT);                           //

  pinMode(PIN_BUTTON1, INPUT);                          //
  pinMode(PIN_BUTTON2, INPUT);                          //
  pinMode(PIN_THERM, INPUT);                            //

  digitalWrite(PIN_IN3, HIGH);                           //
  digitalWrite(PIN_IN4, LOW);                           //

}

void loop() {

  uint8_t buttons = lcd.readButtons();

  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("plateBonder");
  lcd.setCursor(0,1);
  lcd.print("Temp = ");
  lcd.print(temp);

  //if (analogRead(PIN_BUTTON1) < 100 && analogRead(PIN_BUTTON2) < 100){
  //  digitalWrite(PIN_VALVE, HIGH);
  //  digitalWrite(PIN_LED, HIGH);
  //  delay(250);
  //}
  //if (analogRead(PIN_BUTTON1) > 100 || analogRead(PIN_BUTTON2) > 100){
  //  digitalWrite(PIN_VALVE, LOW);
  //  digitalWrite(PIN_LED, LOW);
  //  delay(250);
  //}

  tempRead();
  myPID.Compute();
  analogWrite(PIN_ENB, Output);
  delay(250);
}

void tempRead(){

  // Read-out of analog pin 0 - 3.3V with 12 bit ADC --> values between 0 - 4095. A voltage divider with a Rref of 9960 Ohms is used
  // Vtherm = Vout * Rref / (Vin - Vout) in which the V values are the values for the voltage converted using the 12 bit ADC.
  Vtherm = analogRead(PIN_THERM);
  // Relation between Resistance and temperature is a logaritmic realation:
  // Resistance = 26828 e ^ (-0.04 * temp) (fit in Excel R2 = 0.9991)
  temp = -25*log(Vtherm*9960/(4095-Vtherm)/26828);

}
